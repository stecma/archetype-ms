package com.globalia.application.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.Context;
import com.globalia.application.AbstractSaveDao;
import com.globalia.application.Health;
import com.globalia.dto.KafkaItem;
import com.globalia.enumeration.LogType;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.json.JsonHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RefreshScope
@Slf4j
public class MessageConsumerReply {

	@Value("${logsFormat}")
	private String logsFormat;

	@Autowired
	private Context context;
	@Autowired
	private JsonHandler jsonHandler;
	@Autowired
	private Health health;
	@Autowired
	private RedisLoader redis;

	@KafkaListener(topics = "${kafka.topicname}")
	@SendTo("${kafka.topicreplyname}")
	public String processMessage(final String message) throws InterruptedException {
		MessageConsumerReply.log.info(String.format("Received content: %s", message));
		try {
			KafkaItem item = (KafkaItem) this.jsonHandler.fromJson(message, KafkaItem.class);
			switch (item.getEntity()) {
				case "reload":
					this.redis.launch();
					return "OK";
				default:
					return this.healthProcess(item);
			}
		} catch (IOException e) {
			MessageConsumerReply.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(AbstractSaveDao.EXCEPTION_SER_MSG, e.getMessage()));
		}
		return null;
	}

	private String healthProcess(final KafkaItem item) throws JsonProcessingException {
		item.setJson(this.jsonHandler.toJson(this.health.health()));
		return this.jsonHandler.toJson(item);
	}
}