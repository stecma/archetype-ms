package com.globalia.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.dao.SaveDao;
import com.globalia.dto.credential.ItemResponse;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.infraestructure.SqlLoader;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Slf4j
public abstract class AbstractSaveDao<T> extends SaveDao<T> {

	public static final String EXCEPTION_SER_MSG = "Unknown error during object serialization %s";
	public static final String EXCEPTION_DES_MSG = "Unknown error during object deserialization %s";
	protected static final String CRE_ACTION = "create";
	protected static final String UPD_ACTION = "update";
	protected static final String QUERY_NOT_FOUND = "Query not found %s";

	@Getter
	@Value("${logsFormat}")
	private String logsFormat;

	@Getter
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Getter
	@Autowired
	private SqlLoader sqlLoader;

	protected abstract Object[] getParams(final SqlLoader.sqlType sqlType, final T item) throws JsonProcessingException;

	protected abstract String getSqlFile(final T item);

	protected abstract T getObject(String json) throws IOException;

	protected abstract void setResult(final ItemResponse response, final T item, final SqlLoader.sqlType sqlType);

	public ItemResponse sendToBBDD(final String action, final String json) {
		ItemResponse response = new ItemResponse();
		try {
			switch (action) {
				case CRE_ACTION:
					return this.createItem(this.getObject(json));
				case UPD_ACTION:
					return this.updateItem(this.getObject(json));
				default:
					return this.deleteItem(this.getObject(json));
			}
		} catch (IOException e) {
			AbstractSaveDao.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_DES_MSG, e.getMessage()));
			response.setError(new Error());
		}
		return response;
	}

	public ItemResponse createItem(final T item) {
		return this.execute(SqlLoader.sqlType.INSERT, item);
	}

	public ItemResponse updateItem(final T item) {
		return this.execute(SqlLoader.sqlType.UPDATE, item);
	}

	public ItemResponse deleteItem(final T item) {
		return this.execute(SqlLoader.sqlType.DELETE, item);
	}

	protected ItemResponse execute(final SqlLoader.sqlType sqlType, final T item) {
		ItemResponse response = new ItemResponse();
		try {
			response.setError(this.executeDB(getSqlFile(item), sqlType.name(), getParams(sqlType, item)));
			if (response.getError() == null) {
				this.setResult(response, item, sqlType);
			}
		} catch (JsonProcessingException jpe) {
			AbstractSaveDao.log.error(getLogsFormat(), getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_SER_MSG, jpe.getMessage()));
			response.setError(new Error());
		}
		return response;
	}

	protected Error executeDB(final String file, final String sqlType, final Object[] params) {
		try {
			String query = this.sqlLoader.getSql(file, sqlType);
			if (StringUtils.hasText(query)) {
				this.jdbcTemplate.update(query, params);
				return null;
			}
			AbstractSaveDao.log.error(this.logsFormat, getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(QUERY_NOT_FOUND, sqlType));
		} catch (RuntimeException data) {
			AbstractSaveDao.log.error(this.logsFormat, getContext().getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(EXCEPTION_SER_MSG, data.getMessage()));
		}
		return new Error();
	}
}