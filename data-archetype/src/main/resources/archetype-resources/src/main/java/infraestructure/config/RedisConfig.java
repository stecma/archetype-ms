package com.globalia.infraestructure.config;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.StringUtils;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;

@Configuration
public class RedisConfig {

	@Value("${redis.host}")
	private String redisHost;
	@Value("${redis.port}")
	private int redisPort;
	@Value("${redis.password}")
	private String redisPass;
	@Value("${redis.timeoutSec}")
	private long redisTimeOutSec;
	@Value("${redis.maxTotal}")
	private int redisMaxTotal;
	@Value("${redis.maxIdle}")
	private int redisMaxIdle;
	@Value("${redis.minIdle}")
	private int redisMinIdle;
	@Value("${redis.maxWait}")
	private int redisMaxWait;
	@Value("${redis.slotRedis}")
	private int slotRedis;

	@SuppressWarnings("unchecked")
	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
		redisStandaloneConfiguration.setHostName(this.redisHost);
		redisStandaloneConfiguration.setPort(this.redisPort);
		redisStandaloneConfiguration.setDatabase(this.slotRedis);
		if (StringUtils.hasText(this.redisPass) && !"empty".equalsIgnoreCase(this.redisPass)) {
			redisStandaloneConfiguration.setPassword(RedisPassword.of(this.redisPass));
		}

		JedisClientConfiguration.JedisClientConfigurationBuilder jedisClientConfiguration = JedisClientConfiguration.builder();
		jedisClientConfiguration.connectTimeout(Duration.ofSeconds(this.redisTimeOutSec));
		jedisClientConfiguration.readTimeout(Duration.ofSeconds(this.redisTimeOutSec));

		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration.build());
		this.addPoolConfig(jedisConFactory.getPoolConfig());

		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
		redisTemplate.setDefaultSerializer(new StringRedisSerializer());
		redisTemplate.setConnectionFactory(jedisConFactory);

		return redisTemplate;
	}

	private void addPoolConfig(final GenericObjectPoolConfig<JedisPoolConfig> poolConfig) {
		if (poolConfig != null) {
			poolConfig.setMaxTotal(this.redisMaxTotal);
			poolConfig.setMaxIdle(this.redisMaxIdle);
			poolConfig.setMinIdle(this.redisMinIdle);
			poolConfig.setMaxWaitMillis(this.redisMaxWait);
		}
	}
}