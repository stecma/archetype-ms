package com.globalia.infraestructure;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@RefreshScope
@Service
public class SqlLoader {

	public static final String KEY_FORMAT = "%s¬%s";
	private final ExecutorService pool = Executors.newFixedThreadPool(sqlFile.values().length);
	private final Map<String, Map<String, String>> queriesMap = new HashMap<>();
	private final Map<String, String> queryMap = new HashMap<>();

	// Properties in consul with the SQL sentences.
	@Value("${queriesSQL}")
	private String queriesSQL;

	public void launch() {
		CompletableFuture.allOf(Arrays.stream(sqlFile.values()).map(v -> this.loadSQLQueries(this.getQueriesSQL(v), v)).toArray(CompletableFuture[]::new));
	}

	public String getSql(final String file, final String sqlType) {
		return this.queriesMap.containsKey(file.toLowerCase()) && this.queriesMap.get(file.toLowerCase()).containsKey(sqlType.toLowerCase()) ? this.queriesMap.get(file.toLowerCase()).get(sqlType.toLowerCase()) : null;
	}

	@Async
	CompletableFuture<Void> loadSQLQueries(final String queries, final sqlFile file) {
		return CompletableFuture.supplyAsync(() -> {
			if (StringUtils.hasText(queries)) {
				SqlLoader.log.info(String.format("Init load queries from %s", file.name()));
				if (!queries.equalsIgnoreCase(this.queryMap.get(file.name().toLowerCase()))) {
					this.getSqlSentence(file.name().toLowerCase(), queries);
				}
				SqlLoader.log.info(String.format("Load complete %s", file.name()));
			}
			return null;
		}, this.pool);
	}

	private void getSqlSentence(final String file, final String queries) {
		if (!this.queryMap.containsKey(file)) {
			this.queryMap.put(file, "");
			this.queriesMap.put(file, null);
		}
		this.queryMap.replace(file, queries);

		JsonObject json = (JsonObject) JsonParser.parseString(queries);
		Map<String, String> map = new HashMap<>();
		for (JsonElement query : json.get("queries").getAsJsonArray()) {
			map.computeIfAbsent(query.getAsJsonObject().get("type").getAsString().toLowerCase(), k -> query.getAsJsonObject().get("sql").getAsString());
		}
		this.queriesMap.replace(file, map);
	}

	private String getQueriesSQL(final sqlFile file) {
		switch (file) {
			case QUERY_CONCEPT:
				return this.queriesSQL;
			default:
				return "";
		}
	}

	public enum sqlFile {
		QUERY_CONCEPT(false);

		public final boolean simpleMap;

		sqlFile(final boolean simpleMap) {
			this.simpleMap = simpleMap;
		}
	}

	public enum sqlType {
		QUERY,
		QUERY_ALL,
		QUERY_ALL_SCAN,
		INSERT,
		UPDATE,
		DELETE,
	}
}