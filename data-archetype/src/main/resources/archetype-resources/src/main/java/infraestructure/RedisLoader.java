package com.globalia.infraestructure;

import com.globalia.Context;
import com.globalia.application.repository.GetItems;
import com.globalia.dto.ItemDao;
import com.globalia.enumeration.LogType;
import com.globalia.redis.RedisClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@RefreshScope
@Service
public class RedisLoader {

	public static final String REDIS_KEY_FORMAT = "%s:%s";
	private static final String ERROR_MSG = "Error: %s";
	private final ExecutorService pool = Executors.newFixedThreadPool(SqlLoader.sqlFile.values().length);
	//private final ExecutorService poolMaster = Executors.newFixedThreadPool(MasterType.values().length);

	@Value("${logsFormat}")
	private String logsFormat;
	// Properties in consul with the redis key.
	@Value("${redisKey}")
	private String redisKey;

	@Autowired
	private GetItems getItems;
	@Autowired
	private RedisClient client;
	@Autowired
	private Context context;

	public void launch() {
		CompletableFuture.allOf(Arrays.stream(SqlLoader.sqlFile.values()).map(this::loadRedis).toArray(CompletableFuture[]::new));
	}

	public String getRediskey(final String redisKey, final boolean isNew) {
		String pattern = RedisLoader.REDIS_KEY_FORMAT;
		if (isNew) {
			pattern = "%s:%s_new";
		}
		return String.format(pattern, this.context.getEnvironment(), redisKey);
	}

	@Async
	CompletableFuture<Void> loadRedis(final SqlLoader.sqlFile file) {
		return CompletableFuture.supplyAsync(() -> {
			this.addAllRedis(String.format(RedisLoader.REDIS_KEY_FORMAT, this.getKey(file), file), this.getItems.getItems(file.name(), false), true);
			this.addAllRedis(this.getKey(file), this.getItems.getItems(file), file.simpleMap);
			return null;
		}, this.pool);
	}

	private String getKey(final SqlLoader.sqlFile file) {
		switch (file) {
			case QUERY_CONCEPT:
				return this.redisKey;
			default:
				return "";
		}
	}

	private void addAllRedis(final String baseKey, final List<ItemDao> items, final boolean simpleMap) {
		Map<String, Map<String, Object>> map = this.getMap(items, simpleMap);
		if (!map.isEmpty()) {
			for (Map.Entry<String, Map<String, Object>> entry : map.entrySet()) {
				String finalKey = baseKey;
				if (StringUtils.hasText(entry.getKey())) {
					finalKey = String.format(RedisLoader.REDIS_KEY_FORMAT, baseKey, entry.getKey());
				}
				this.addMap(finalKey, entry.getValue());
			}
		}
	}

	private Map<String, Map<String, Object>> getMap(final List<ItemDao> items, final boolean simpleMap) {
		Map<String, Map<String, Object>> map = new HashMap<>();
		if (!items.isEmpty()) {
			String keyMap = "";
			for (ItemDao item : items) {
				String key = item.getId();
				if (!simpleMap) {
					String[] keys = item.getId().split(":");
					keyMap = keys[0];
					key = keys[1];
				}

				map.computeIfAbsent(keyMap, k -> new HashMap<>());
				map.get(keyMap).computeIfAbsent(key, k -> item.getValue());
			}
		}
		return map;
	}

	private void addMap(final String finalKey, final Map<String, Object> map) {
		try {
			String temporaryKey = this.getRediskey(finalKey, true);
			String redisKey = this.getRediskey(finalKey, false);
			RedisLoader.log.info(String.format("Load redis %s", temporaryKey));
			this.client.addMap(temporaryKey, map);
			this.client.rename(temporaryKey, redisKey);
			RedisLoader.log.info(String.format("Completed %s", redisKey));
		} catch (RuntimeException r) {
			RedisLoader.log.error(this.logsFormat, this.context.getEnvironment(), LogType.ERROR.name(), null, null, null, null, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(RedisLoader.ERROR_MSG, r.getLocalizedMessage()));
		}
	}
}