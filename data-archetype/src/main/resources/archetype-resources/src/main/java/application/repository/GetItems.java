package com.globalia.application.repository;

import com.globalia.Context;
import com.globalia.application.mapper.ItemMapper;
import com.globalia.aspect.Dao;
import com.globalia.dto.ItemDao;
import com.globalia.infraestructure.SqlLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

@Dao
@Component
@Slf4j
public class GetItems {

	@Value("${logsFormat}")
	private String logsFormat;

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private SqlLoader sqlLoader;
	@Autowired
	private Context context;

	public List<ItemDao> getItems(final SqlLoader.sqlFile file) {
		return this.getItems(file.name(), false);
	}

	public List<ItemDao> getItems(final String file, final boolean scanId) {
		SqlLoader.sqlType sqlType = SqlLoader.sqlType.QUERY_ALL_SCAN;
		if (!scanId) {
			sqlType = SqlLoader.sqlType.QUERY_ALL;
		}
		List<ItemDao> items = null;
		String query = this.sqlLoader.getSql(file, sqlType.name());
		if (StringUtils.hasText(query)) {
			items = this.jdbcTemplate.query(query, new ItemMapper());
		}
		return items;
	}
}