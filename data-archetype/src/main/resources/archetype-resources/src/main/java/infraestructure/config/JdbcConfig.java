package com.globalia.infraestructure.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class JdbcConfig {

	@Value("${oracle.className}")
	private String className;
	@Value("${oracle.databaseUrl}")
	private String databaseUrl;
	@Value("${oracle.schema}")
	private String schema;
	@Value("${oracle.schemaPass}")
	private String schemaPass;
	@Value("${oracle.maximumPoolSize}")
	private int maximumPoolSize;
	@Value("${oracle.timeOutConnMs}")
	private int timeOutConnMs;

	@Bean
	public DataSource getDataSource() {
		HikariConfig config = new HikariConfig();
		config.setDriverClassName(this.className);
		config.setJdbcUrl(this.databaseUrl);
		config.setUsername(this.schema);
		config.setPassword(this.schemaPass);
		config.setMaximumPoolSize(this.maximumPoolSize);
		config.setConnectionTimeout(this.timeOutConnMs);
		return new HikariDataSource(config);
	}

	@Bean
	public JdbcTemplate jdbcTemplate() {
		JdbcTemplate template = new JdbcTemplate();
		template.setDataSource(this.getDataSource());

		return template;
	}
}