package com.globalia.infraestructure;

import com.globalia.HelperTest;
import com.globalia.dto.ItemDao;
import com.globalia.infraestructure.SqlLoader.sqlFile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class RedisLoaderTest extends HelperTest {

	@InjectMocks
	private RedisLoader redisLoader;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		init(RedisLoader.class, this.redisLoader, "redisKey", "redisKey");
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testLoaderNoItems() {
		when(this.getItems.getItems(any())).thenReturn(new ArrayList<>());
		this.redisLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testLoaderRuntimeException() {
		ItemDao item = new ItemDao();
		item.setId("1");
		item.setValue("json");

		ItemDao master = new ItemDao();
		master.setId("1¬COMPANY");
		master.setValue("json");

		when(this.getItems.getItems(any())).thenReturn(Collections.singletonList(item));
		when(this.getItems.getItems(sqlFile.QUERY_CONCEPT)).thenReturn(Collections.singletonList(master));
		doThrow(RuntimeException.class).when(this.client).addMap(anyString(), anyMap());
		this.redisLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testLoader() {
		ItemDao item = new ItemDao();
		item.setId("1");
		item.setValue("json");

		ItemDao master = new ItemDao();
		master.setId("1¬COMPANY");
		master.setValue("json");

		when(this.getItems.getItems(any())).thenReturn(Collections.singletonList(item));
		this.redisLoader.launch();
		assertTrue(true);
	}
}