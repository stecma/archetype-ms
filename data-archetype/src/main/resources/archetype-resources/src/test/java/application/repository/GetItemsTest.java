package com.globalia.application.repository;

import com.globalia.HelperTest;
import com.globalia.application.mapper.ItemMapper;
import com.globalia.dto.ItemDao;
import com.globalia.infraestructure.SqlLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GetItemsTest extends HelperTest {

	@InjectMocks
	private GetItems getItems;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemsNoQuery() {
		when(this.sqlLoader.getSql(any(), any())).thenReturn(null);
		assertNull(this.getItems.getItems(SqlLoader.sqlFile.QUERY_CONCEPT));
	}

	@Test
	public void testGetItems() {
		when(this.sqlLoader.getSql(any(), any())).thenReturn("query");
		when(this.jdbcTemplate.query(anyString(), any(ItemMapper.class))).thenReturn(Collections.singletonList(new ItemDao()));
		assertNotNull(this.getItems.getItems(SqlLoader.sqlFile.QUERY_CONCEPT));
	}
}

