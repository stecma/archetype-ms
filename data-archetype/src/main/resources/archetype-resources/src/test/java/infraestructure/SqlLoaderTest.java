package com.globalia;

import com.globalia.infraestructure.SqlLoader;
import com.globalia.infraestructure.SqlLoader.sqlFile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SqlLoaderTest extends HelperTest {

	@InjectMocks
	private SqlLoader sqlLoader;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testLaunch() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", null);
		this.sqlLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testLoaderNoQuery() {
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", null);
		this.sqlLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testLoader() {
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", HelperTest.QUERIES);
		this.sqlLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testLoaderReload() {
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", HelperTest.QUERIES_EMPTY);
		this.sqlLoader.launch();
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", HelperTest.QUERIES);
		this.sqlLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testLoaderReloadEqualsQuery() {
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", HelperTest.QUERIES);
		this.sqlLoader.launch();
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", HelperTest.QUERIES);
		this.sqlLoader.launch();
		assertTrue(true);
	}

	@Test
	public void testGetSqlNoFile() {
		assertNull(this.sqlLoader.getSql(sqlFile.QUERY_CONCEPT.name(), SqlLoader.sqlType.QUERY_ALL.name()));
	}

	@Test
	public void testGetSqlNoQuery() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", HelperTest.QUERIES);
		this.sqlLoader.launch();
		TimeUnit.SECONDS.sleep(2);
		assertNull(this.sqlLoader.getSql(sqlFile.QUERY_CONCEPT.name(), "dummy"));
	}

	@Test
	public void testGetSql() throws InterruptedException {
		init(SqlLoader.class, this.sqlLoader, "queriesSQL", HelperTest.QUERIES);
		this.sqlLoader.launch();
		TimeUnit.SECONDS.sleep(2);
		assertNotNull(this.sqlLoader.getSql(sqlFile.QUERY_CONCEPT.name(), SqlLoader.sqlType.QUERY.name()));
	}
}
