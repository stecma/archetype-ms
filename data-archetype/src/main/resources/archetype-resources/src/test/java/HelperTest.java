package com.globalia;

import com.globalia.application.repository.GetItems;
import com.globalia.dto.KafkaItem;
import com.globalia.infraestructure.RedisLoader;
import com.globalia.infraestructure.SqlLoader;
import com.globalia.json.JsonHandler;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.RedisClient;
import org.mockito.Mock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.HashSet;

public abstract class HelperTest {

	protected static final String QUERIES = "{\"queries\": [ {\"type\": \"query\", \"sql\": \"QUERY\"}, {\"type\": \"query_all\", \"sql\": \"QUERY\"},{\"type\": \"insert\", \"sql\": \"QUERY\"},{\"type\": \"update\", \"sql\": \"QUERY\"}, {\"type\": \"delete\", \"sql\": \"QUERY\"}, { \"type\": \"insert¬connector¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"update¬connector¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"delete¬connector¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"insert¬group¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"update¬group¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"delete¬group¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"insert¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"update¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"delete¬supplier\", \"sql\": \"QUERY\" }, { \"type\": \"insert¬credential\", \"sql\": \"QUERY\" }, { \"type\": \"update¬credential\", \"sql\": \"QUERY\" }, { \"type\": \"delete¬credential\", \"sql\": \"QUERY\" }, { \"type\": \"assign¬credential\", \"sql\": \"QUERY\" }]}";
	protected static final String QUERIES_EMPTY = "{ \"queries\": [ { \"type\": \"dummy\", \"sql\": \"QUERY\" } ] }";

	@Mock
	protected JsonHandler jsonHandler;
	@Mock
	protected Context context;
	@Mock
	protected RedisClient client;
	@Mock
	protected JdbcTemplate jdbcTemplate;
	@Mock
	protected SqlLoader sqlLoader;
	@Mock
	protected GetItems getItems;
	@Mock
	protected RedisLoader redisLoader;

	public static KafkaItem message(final String entity) {
		KafkaItem kafka = new KafkaItem();
		kafka.setEntity(entity);
		kafka.setAction("test");
		kafka.setJson("json");

		return kafka;
	}

	public static Monitor monitor() {
		Monitor monitor = new Monitor();
		monitor.setLogs(new HashSet<>());
		monitor.setStats(new HashSet<>());
		return monitor;
	}

	public static void init(final Class<?> classes, final Object obj, final String fieldName, final String fieldValue) {
		if (StringUtils.hasText(fieldName)) {
			try {
				Field field = classes.getDeclaredField(fieldName);
				field.setAccessible(true);
				field.set(obj, fieldValue);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}