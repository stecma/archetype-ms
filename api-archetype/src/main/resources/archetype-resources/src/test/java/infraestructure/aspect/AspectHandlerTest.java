package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.enumeration.StatType;
import com.globalia.monitoring.MonitorHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class AspectHandlerTest extends HelperTest {

	@InjectMocks
	private AspectHandler aspect;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private MethodSignature signature;
	@Mock
	private MonitorHandler handler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test
	public void testExecuteNoMonitor() throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn("create");
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.aspect.execute(this.joinPoint, null));
	}

	@Test
	public void testExecuteNoMessge() throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn("create");
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.aspect.execute(this.joinPoint, HelperTest.monitor(), null, null, null));
	}

	@Test
	public void testExecute() throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn("create");
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.aspect.execute(this.joinPoint, HelperTest.monitor(), StatType.INIT, "test", "test"));
	}
}