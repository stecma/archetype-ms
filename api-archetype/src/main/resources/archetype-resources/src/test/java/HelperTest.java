package com.globalia;

import com.globalia.api.credential.ApiRQ;
import com.globalia.application.producer.MessageProducer;
import com.globalia.dto.KafkaItem;
import com.globalia.dto.PairValue;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.TradePolicyItem;
import com.globalia.enumeration.credential.MasterType;
import com.globalia.enumeration.credential.PercentageType;
import com.globalia.monitoring.Monitor;
import org.mockito.Mock;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class HelperTest {

	@Mock
	protected MessageProducer producer;

	public static ApiRQ tradeRQ() {
		ApiRQ request = new ApiRQ();
		request.setTrade(trade());
		request.setMonitor(monitor());
		return request;
	}

	public static TradePolicyItem trade() {
		TradePolicyItem trade = new TradePolicyItem();
		trade.setId("1¬1¬1¬1¬1¬1¬1¬1¬1¬");
		trade.setCredential("1");
		trade.setPercentage(1D);
		trade.setPercentageType(PercentageType.MARKUP);
		trade.setCodes(new HashSet<>(Set.of(pairValue(MasterType.SERVICE_TYPE, "1"))));
		trade.setMasters(new HashSet<>(Set.of(master(MasterType.SERVICE_TYPE, "1"))));

		return trade;
	}

	public static MasterItem master(final MasterType type, final String value) {
		MasterItem child = new MasterItem();
		child.setId(value);
		child.setNames(new LinkedHashSet<>());
		child.setEntity(type.name());
		return child;
	}


	private static PairValue pairValue(final MasterType type, final String value) {
		PairValue pair = new PairValue();
		pair.setKey(type.name());
		pair.setValue(new LinkedHashSet<>(Set.of(value)));
		return pair;
	}

	public static Monitor monitor() {
		Monitor monitor = new Monitor();
		monitor.setLogs(new HashSet<>());
		monitor.setStats(new HashSet<>());
		return monitor;
	}

	public static KafkaItem message() {
		KafkaItem kafka = new KafkaItem();
		kafka.setKey("key");
		kafka.setEntity("entity");
		kafka.setAction("test");
		kafka.setJson("json");

		return kafka;
	}

	public static void init(final Class<?> classes, final Object obj, final String fieldName, final String fieldValue, final boolean isInit) {
		if (StringUtils.hasText(fieldName)) {
			try {
				Field field = classes.getDeclaredField(fieldName);
				field.setAccessible(true);
				field.set(obj, fieldValue);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		if (isInit) {
			try {
				Method postConstruct = classes.getDeclaredMethod("init");
				postConstruct.setAccessible(true);
				postConstruct.invoke(obj);
			} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}
}