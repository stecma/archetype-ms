package com.globalia.application.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.dto.KafkaItem;
import com.globalia.json.JsonHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MessageProducerTest extends HelperTest {

	@InjectMocks
	private MessageProducer producer;
	@Mock
	private KafkaTemplate<String, KafkaItem> kafkaTemplate;
	@Mock
	private ReplyingKafkaTemplate<String, String, String> replyKafkaTemplate;
	@Mock
	protected JsonHandler jsonHandler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testReplyException() {
		assertNull(this.producer.reply(message()));
	}

	@Test
	public void testReplyJsonProcessingException() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNull(this.producer.reply(message()));
	}

	@Test
	public void testReplyNullResponse() throws JsonProcessingException {
		init(MessageProducer.class, this.producer, "topicName", "topic1", false);
		init(MessageProducer.class, this.producer, "replyTopic", "topic2", false);

		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.replyKafkaTemplate.sendAndReceive(any())).thenReturn(new RequestReplyFuture<>());
		assertNull(this.producer.reply(message()));
	}

	@Test
	public void testReplyGetResponseNull() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(null);
		assertNull(this.producer.getResponse("test"));
	}

	@Test
	public void testReplyGetResponse() throws IOException {
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(message());
		assertNotNull(this.producer.getResponse("test"));
	}
}
