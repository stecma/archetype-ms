package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.enumeration.StatType;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CoreAspectTest extends HelperTest {

	@InjectMocks
	private CoreAspect coreAspect;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private MethodSignature signature;
	@Mock
	private AspectHandler handler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test
	public void testLoggerService() throws Throwable {
		when(this.handler.execute(any(), any(), nullable(StatType.class), anyString(), anyString())).thenReturn("OK");
		assertNotNull(this.coreAspect.loggerService(this.joinPoint, monitor()));
	}

	@Test
	public void testLoggerRedisSelect() throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn("getItem");
		when(this.handler.execute(any(), any(), any(), anyString(), anyString())).thenReturn("OK");
		assertNotNull(this.coreAspect.loggerRedis(this.joinPoint, monitor()));
	}

	@Test
	public void testLoggerRedisAddItem() throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn("addItem");
		when(this.handler.execute(any(), any(), any(), anyString(), anyString())).thenReturn("OK");
		assertNotNull(this.coreAspect.loggerRedis(this.joinPoint, HelperTest.monitor()));
	}

}