package com.globalia.infraestructure.rest.converter;

import com.globalia.api.credential.ApiRQ;
import com.globalia.dto.credential.AllItemsResponse;
import com.globalia.infraestructure.AbstractConverter;
import com.globalia.service.IFindAll;
import org.springframework.stereotype.Component;

@Component
public class AllItemsConverter extends AbstractConverter {

	public Object getAllItems(final ItemConverter.apiRequest entity, final ApiRQ request) {
		AllItemsResponse response = (AllItemsResponse) this.getFindAll(entity).getAllItems(getRequest(entity, request), request.getMonitor());
		return response.getError() != null ? response.getError() : this.getResponse(entity, response);
	}

	private Object getResponse(final ItemConverter.apiRequest entity, final AllItemsResponse response) {
		return null;
	}

	private IFindAll getFindAll(final ItemConverter.apiRequest entity) {
		return null;
	}
}