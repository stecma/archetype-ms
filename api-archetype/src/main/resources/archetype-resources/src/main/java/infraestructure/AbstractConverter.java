package com.globalia.infraestructure;

import com.globalia.api.BaseApiRQ;
import com.globalia.dto.BaseItemResponse;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.springframework.cloud.context.config.annotation.RefreshScope;

public abstract class AbstractConverter {

	protected Object getRequest(final ItemConverter.apiRequest entity, final BaseApiRQ request) {
		return null;
	}

	protected Object getResponse(final ItemConverter.apiRequest entity, final BaseItemResponse response) {
		return null;
	}
}