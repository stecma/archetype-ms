package com.globalia.infraestructure.rest.converter;

import com.globalia.api.BaseApiRQ;
import com.globalia.dto.BaseItemResponse;
import com.globalia.infraestructure.AbstractConverter;
import com.globalia.service.ISave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class SaveConverter extends AbstractConverter {

	public Object save(final ItemConverter.apiRequest entity, final saveAction action, final BaseApiRQ request) {
		BaseItemResponse response;
		switch (action) {
			case CREATE:
				response = this.getSave(entity).createItem(getRequest(entity, request), request.getMonitor());
				break;
			case UPDATE:
				response = this.getSave(entity).updateItem(getRequest(entity, request), request.getMonitor());
				break;
			default:
				response = this.getSave(entity).deleteItem(getRequest(entity, request), request.getMonitor());
		}

		return response.getError() != null ? response.getError() : getResponse(entity, response);
	}

	private ISave getSave(final ItemConverter.apiRequest entity) {
		return null;
	}

	public enum saveAction {
		CREATE,
		UPDATE,
		DELETE
	}
}