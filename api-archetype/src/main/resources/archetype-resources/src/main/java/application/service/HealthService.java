package com.globalia.application.service;

import com.globalia.Context;
import com.globalia.dto.KafkaItem;
import com.globalia.application.producer.MessageProducer;
import com.globalia.redis.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.UUID;

@Service
public class HealthService implements HealthIndicator {

	@Autowired
	private RedisClient client;
	@Autowired
	private Context context;
	@Autowired
	private MessageProducer producer;

	@Override
	public Health health() {
		if (this.redisCheck() < 0) {
			return Health.down().withDetail("Error redis connection", 500).build();
		}
		String reply = this.sendMessage();
		if (!StringUtils.hasText(reply) || reply.toUpperCase().contains(Status.DOWN.getCode())) {
			return Health.down().withDetail("Sync service connection", 500).build();
		}
		return Health.up().build();
	}

	private String sendMessage() {
		KafkaItem message = new KafkaItem();
		message.setKey(UUID.randomUUID().toString());
		message.setEntity("health");
		return this.producer.reply(message);
	}

	private int redisCheck() {
		try {
			return this.client.patternKeys(String.format("%s:*", this.context.getEnvironment())).size();
		} catch (RuntimeException data) {
			return -1;
		}
	}
}