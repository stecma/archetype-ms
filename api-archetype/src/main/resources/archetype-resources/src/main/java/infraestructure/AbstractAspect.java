package com.globalia.infraestructure;

import com.globalia.api.credential.ApiRQ;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

public abstract class AbstractAspect {

	/**
	 * Request validation.
	 *
	 * @param entity  API entity.
	 * @param request RQ model.
	 */
	protected void validateRequest(final String entity, final ApiRQ request) {
		if (request == null || !isValidRQ(entity, request)) {
			throw new ValidateException(new Error("Invalid request", ErrorLayer.API_LAYER), null);
		}
	}

	/**
	 * Id validation.
	 *
	 * @param entity  API entity.
	 * @param request RQ model.
	 * @return False if the identifier is empty.
	 */
	protected boolean isEmptyID(final String entity, final ApiRQ request) {
		return false;
	}

	/**
	 * Content validation.
	 *
	 * @param entity  API entity.
	 * @param request RQ model.
	 */
	protected void validateContent(final String entity, final ApiRQ request) {
		this.validateContent(entity, request, false);
	}

	protected void validateContent(final String entity, final ApiRQ request, final boolean done) {}

	private boolean isValidRQ(final String entity, final ApiRQ request) {
		return false;
	}
}